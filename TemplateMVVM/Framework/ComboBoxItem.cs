﻿/*
    public ObservableCollection<ComboBoxItem> Test { get; set; }
    public ComboBoxItem TestValue { get; set; }

    // Инициализация
    public Init() {
        Test = new ObservableCollection<ComboBoxItem>() {
            new ComboBoxItem(){ Id = 1, Name = "Name1"},
            new ComboBoxItem(){ Id = 2, Name = "Name2"}
        };
    }

    // Получить текущее значение ComboBox
    var getVal = TestValue.Id;
*/

/*
    <ComboBox ItemsSource="{Binding Path=Test}" SelectedItem="{Binding Path=TestValue}" SelectedIndex="8" DisplayMemberPath="Name"/>
*/



namespace TemplateMVVM.Framework {
    class ComboBoxItem {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
