﻿using System;
using System.Windows.Input;


/*
    public Command StartCommand { get; }

    // Инициализация
    public Init() {
        testCommand = new Command(delegate(object obj) {
                // Клик
        });
    }
*/

/*
    <Button Content="Test" Command="{Binding TestCommand}" Width="100" Height="38"/>
*/


namespace TemplateMVVM.Framework {
    public class Command : ICommand {

        public Command(Action<object> action) {
            ExecuteDelegate = action;
        }


        // Свойства
        #region Properties
        public Predicate<object> CanExecuteDelegate { get; set; }
        public Action<object> ExecuteDelegate { get; set; }
        #endregion





        public bool CanExecute(object parameter) {
            if(CanExecuteDelegate != null) {
                return CanExecuteDelegate(parameter);
            }

            return true;
        }





        public event EventHandler CanExecuteChanged {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }





        public void Execute(object parameter) {
            ExecuteDelegate?.Invoke(parameter);
        }
    }
}
