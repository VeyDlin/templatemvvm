﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;

using TemplateMVVM.ViewModel.Properties;
using TemplateMVVM.AppProperties;
using TemplateMVVM.Framework;


namespace TemplateMVVM.ViewModel {
    class MainViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;


        #region Глобальные свойства
        static GlobalAppProperty globalAppProperty;
        #endregion


        #region Свойства
        public MainProperty properties { get; private set; }
        #endregion


        #region Команды
        public Command TestCommand { get; }
        #endregion




        // Инициализация
        public MainViewModel() {

            // --- Глобальные свойства
            globalAppProperty = GlobalAppProperty.GetInstance();

            // --- Свойства
            properties = new MainProperty();

            // --- Команды
            TestCommand = new Command(delegate(object obj) {
                // Клик
            });

        }

    }
}












