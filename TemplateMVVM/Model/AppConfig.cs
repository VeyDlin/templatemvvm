﻿using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Text;


namespace TemplateMVVM.Model {
    static class AppConfig {

        public class Properties {

        };


        private static string configName = "appConfig.json";

        public static Properties config { get; private set; }


        public static void Init() {
            config = new Properties();

            #if DEBUG
                if(File.Exists(configName)) {
                    File.Delete(configName);
                }
            #endif

            if(!File.Exists(configName)) {
                string jsonText = JsonConvert.SerializeObject(config);
                jsonText = FormatJson(jsonText);
                File.WriteAllText(configName, jsonText);
            }

            string fileText = File.ReadAllText(configName);
            config = JsonConvert.DeserializeObject<Properties>(fileText);
        }





        public static string FormatJson(string str, string indentString = "\t") {
            var indent = 0;
            var quoted = false;
            var sb = new StringBuilder();

            for (var i = 0; i < str.Length; i++) {
                var ch = str[i];
                switch (ch) {
                    case '{':
                    case '[':
                        sb.Append(ch);
                        if (!quoted) {
                            sb.AppendLine();
                            foreach (var e in Enumerable.Range(0, ++indent)) {
                                sb.Append(indentString);
                            }
                        }
                        break;

                    case '}':
                    case ']':
                        if (!quoted) {
                            sb.AppendLine();
                            foreach (var e in Enumerable.Range(0, --indent)) {
                                sb.Append(indentString);
                            }
                        }
                        sb.Append(ch);
                        break;

                    case '"':
                        sb.Append(ch);
                        bool escaped = false;
                        var index = i;
                        while (index > 0 && str[--index] == '\\') {
                            escaped = !escaped;
                        }

                        if (!escaped) {
                            quoted = !quoted;
                        }
                        break;

                    case ',':
                        sb.Append(ch);
                        if (!quoted) {
                            sb.AppendLine();

                            foreach (var e in Enumerable.Range(0, indent)) {
                                sb.Append(indentString);
                            }
                        }
                        break;

                    case ':':
                        sb.Append(ch);
                        if (!quoted) {
                            sb.Append(" ");
                        }
                        break;

                    default:
                        sb.Append(ch);
                        break;
                }
            }
            return sb.ToString();
        }


    }
}
