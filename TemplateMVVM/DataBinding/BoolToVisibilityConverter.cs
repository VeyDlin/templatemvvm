﻿using System;
using System.Windows;
using System.Windows.Data;


//  Visibility="{Binding isSet, Converter={StaticResource BoolToVisibilityConverter}, ConverterParameter=true}"

namespace TemplateMVVM.DataBinding {
    public class BoolToVisibilityConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            var flag = false;
            if (value is bool) {
                flag = (bool)value;
            } else if (value is bool?) {
                var nullable = (bool?)value;
                flag = nullable.GetValueOrDefault();
            }
            if (parameter != null) {
                if (bool.Parse((string)parameter)) {
                    flag = !flag;
                }
            }
            if (flag) {
                return Visibility.Visible;
            } else {
                return Visibility.Collapsed;
            }
        }



        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            var back = ((value is Visibility) && (((Visibility)value) == Visibility.Visible));
            if (parameter != null) {
                if ((bool)parameter) {
                    back = !back;
                }
            }
            return back;
        }

    }
}
