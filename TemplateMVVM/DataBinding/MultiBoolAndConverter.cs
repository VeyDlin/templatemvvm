﻿using System;
using System.Globalization;
using System.Windows.Data;

/*
 <Button Content="Стоп" HorizontalAlignment="Left" Margin="94,194,0,0" VerticalAlignment="Top" Width="46">
   <Button.IsEnabled>
       <MultiBinding Converter="{StaticResource MultiBoolAndConverter}">
          <Binding Path="FirstName"/>
          <Binding Path="LastName"/>
       </MultiBinding>
   </Button.IsEnabled>
</Button>
*/

namespace TemplateMVVM.DataBinding {
    public class MultiBoolAndConverter : IMultiValueConverter {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            foreach(bool element in values) {
                if(element == false) {
                    return false;
                }
            }

            return true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
            throw new NotSupportedException();
        }
    }
}
