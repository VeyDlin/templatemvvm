﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media;

using TemplateMVVM.Model;
using TemplateMVVM.View;

namespace TemplateMVVM {
    public partial class App : Application {
        //SplashScreen splash = new SplashScreen("/Resources/SplashScreen.png");


        public App() {

            // Инициализация JSON конфига
            AppConfig.Init();

            // Если приложение уже запущено
            if (!InstanceCheck()) {
                Current.Shutdown();
                return;
            }


            var StartView = new MainView();
            //splash.Show(false);
            StartView.Show();
        }



        // Проверка на существования экземпляра программы
        static Mutex instanceCheckMutex;
        static bool InstanceCheck() {
            bool isNew;
            instanceCheckMutex = new Mutex(true, "TemplateMVVMApp-" + Assembly.GetExecutingAssembly().GetName().Version.ToString(), out isNew);
            return isNew;
        }




        private void StartUp(object sender, StartupEventArgs e) {
            //splash.Show(true);
        }
    }
}
