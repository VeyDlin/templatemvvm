﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace TemplateMVVM.AppProperties {
    public class GlobalAppProperty : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;


        private static GlobalAppProperty self = null;
        public static ref GlobalAppProperty GetInstance() {
            if(self == null) {
                self = new GlobalAppProperty();
            }
            return ref self;
        }



        // public string test { get; set; }


    }
}
